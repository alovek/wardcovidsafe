import {DEFAULT_CHAPEL} from '../stakeConfig.json';

const initialState = {
  name: '',
  additionalNames: [],
  phone: '',
  address: '',
  reason: '',
  ward: '',
  chapel: DEFAULT_CHAPEL,
  rememberDetails: true,
  lastDateSigned: null,
  includeFood: false,
  includeGames: false,
  checkedInIds: [],
};

export default (state = initialState, action) => {
  switch(action.type) {
    case 'CLEAN_FORM':
      return {...state, ...initialState, chapel: state.chapel}; // important to remember the chapel we are at
    case 'MEMBER_FIELD_UPDATED':
      return {...state, [action.data.field]: action.data.value};
    default:
      return state;
  }
}