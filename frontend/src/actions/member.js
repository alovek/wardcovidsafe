import moment     from 'moment';
import axios      from 'axios';
import backendAPI from '../api';

export const updateField = (data) => async (dispatch, getState) => {
  dispatch({
    type: 'MEMBER_FIELD_UPDATED',
    data,
  })
};

export const checkOutLogs = () => async (dispatch, getState) => {
  // deactivate logs
  const {checkedInIds} = getState().member;

  await backendAPI.post('/deactivate-log', {checkedInIds});

  dispatch({
    type: 'MEMBER_FIELD_UPDATED',
    data: {field: 'lastDateSigned', value: null},
  })

  dispatch({
    type: 'MEMBER_FIELD_UPDATED',
    data: {field: 'checkedInIds', value: []},
  })
};

export const addAnotherHouseholdName = (name) => async (dispatch, getState) => {
  // send off new name with data to backend
  const {phone, address, reason, ward, chapel, checkedInIds} = getState().member;
  const response = await backendAPI.post('/log', {
    name,
    phone,
    address,
    reason,
    ward,
    chapel,
    additionalNames: [],
  });

  dispatch({
    type: 'MEMBER_FIELD_UPDATED',
    data: {field: 'checkedInIds', value: [...checkedInIds, ...response.data.checkedInIds]},
  })
};

export const sendForm = () => async (dispatch, getState) => {
  const member = getState().member;
  const {name, phone, address, ward, additionalNames, chapel, includeFood, includeGames} = member;
  const updatedAdditionalNames = additionalNames.filter(aName => !!aName);

  // handle additional information being added to the reason
  let reason = member.reason;

  if (includeFood) reason = `${reason} (food)`;
  if (includeGames) reason = `${reason} (games)`;

  // send off form to backend
  const response = await backendAPI.post('/log', {
    name,
    phone,
    address,
    reason,
    ward,
    chapel,
    additionalNames: updatedAdditionalNames
  });


  // TODO figure out when the best time to clean the form is
  // if (!member.rememberDetails) {
  //   dispatch({
  //     type: 'CLEAN_FORM',
  //     data: {rememberDetails: false},
  //   })
  // }


  dispatch({
    type: 'MEMBER_FIELD_UPDATED',
    data: {field: 'additionalNames', value: updatedAdditionalNames},
  });

  dispatch({
    type: 'MEMBER_FIELD_UPDATED',
    data: {field: 'lastDateSigned', value: moment().valueOf()},
  })

  dispatch({
    type: 'MEMBER_FIELD_UPDATED',
    data: {field: 'checkedInIds', value: response.data.checkedInIds},
  })
};


export const clearForm = () => async (dispatch, getState) => {
    dispatch({
      type: 'CLEAN_FORM',
    })
};