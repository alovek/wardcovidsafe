const CONFIG = require('./stakeConfig.json');

module.exports = {
  apps : [{
    name: `${CONFIG.DATABASE}-BACKEND`,
    script: 'index.js',
    env_production : {
      NODE_ENV: 'production'
    },
    watch: '.'
  }],
};
