import Checkbox          from '@material-ui/core/Checkbox';
import FormControlLabel  from '@material-ui/core/FormControlLabel';
import IconButton        from '@material-ui/core/IconButton';
import Popover           from '@material-ui/core/Popover';
import FilterListIcon               from '@material-ui/icons/FilterList';
import React, {useEffect, useState} from 'react';
import CONFIG                       from './../stakeConfig.json';
import './LogFilter.css';

const LogFilter = ({wards, setWards, reasons, setReasons, availableWards, showWards}) => {
  const [filterAnchor, setFilterAnchor] = useState(null);

  return (
    <React.Fragment>
      <IconButton color="primary" aria-label="upload picture" component="span" onClick={handleOpenFilter}>
        <FilterListIcon />
      </IconButton>
      <Popover
        open={!!filterAnchor}
        anchorEl={filterAnchor}
        onClose={() => setFilterAnchor(null)}
        anchorOrigin={{
          vertical: 'center',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'center',
          horizontal: 'left',
        }}
      >
        <div className="LogFilter">
          {showWards &&
            <React.Fragment>
              <div className="LogFilter__title">Wards</div>
              {availableWards.map((aWard, idx) => (
                <div className="LogFilter__checkbox" key={idx}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={wards.includes(aWard) || false}
                        onChange={(e, checked) => handleWardUpdate(aWard, checked)}
                        color="primary"
                      />
                    }
                    label={aWard}
                  />
                </div>
              ))}
            </React.Fragment>
          }
          <div className="LogFilter__title">Purposes</div>
          {CONFIG.REASONS.map((aReason, idx) => (
            <div className="LogFilter__checkbox" key={idx}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={reasons.includes(aReason) || false}
                    onChange={(e, checked) => handleReasonUpdate(aReason, checked)}
                    color="primary"
                  />
                }
                label={aReason}
              />
            </div>
          ))}
        </div>
      </Popover>
    </React.Fragment>
  );

  function handleWardUpdate(aWard, checked) {
    let updatedWards = [...wards];

    if (checked && !wards.includes(aWard)) {
      updatedWards.push(aWard);
    }

    if (!checked && wards.includes(aWard)) {
      updatedWards = updatedWards.filter(w => w !== aWard);
    }

    setWards(updatedWards);
  }

  function handleReasonUpdate(aReason, checked) {
    let updateReasons = [...reasons];

    if (checked && !reasons.includes(aReason)) {
      updateReasons.push(aReason);
    }

    if (!checked && reasons.includes(aReason)) {
      updateReasons = updateReasons.filter(r => r !== aReason);
    }

    setReasons(updateReasons);
  }

  function handleOpenFilter(event) {
    setFilterAnchor(event.currentTarget);
  }
};

export default LogFilter;