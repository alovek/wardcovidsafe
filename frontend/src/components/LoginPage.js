import Button                                           from '@material-ui/core/Button';
import Paper                                            from '@material-ui/core/Paper';
import Snackbar                                         from '@material-ui/core/Snackbar';
import TextField                                        from '@material-ui/core/TextField';
import ArrowForwardRoundedIcon                          from '@material-ui/icons/ArrowForwardRounded';
import MuiAlert                                         from '@material-ui/lab/Alert';
import React, {useEffect, useState}                     from 'react';
import './LoginPage.css';
import {connect}                                        from 'react-redux';
import {useHistory}                                     from 'react-router-dom';
import {bindActionCreators}                             from 'redux';
import {login}                                          from '../actions/admin';
import {addAnotherHouseholdName, sendForm, updateField} from '../actions/member';
import {ACCESS_LEVEL}                                   from '../constants';
import SiteMenu                                         from './SiteMenu';


const LoginPage = ({login, admin}) => {
  const history = useHistory();

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [showSnack, setShowSnack] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  useEffect(() => {
    // redirect if logged in
    if (!admin.loggedIn) return;

    if (admin.stakeAccess) {
      history.push('/dashboard');
    } else {
      history.push('/chapel-logs');
    }
  }, [admin.loggedIn]);

  return (
    <div className="LoginPage">
      <SiteMenu />
      <div className="LoginPage__content">
        <Paper elevation={3} className="LoginPage__paper">
          <div className="LoginPage__form">
            <div className="LoginPage__title">
              Login
            </div>
            <div className="LoginPage__fields">
              <div className="LoginPage__input">
                <TextField label="Username" fullWidth value={username} onChange={(e) => setUsername(e.target.value)} />
              </div>
              <div className="LoginPage__input">
                <TextField label="Password" type="password" fullWidth value={password} onChange={(e) => setPassword(e.target.value)} />
              </div>
              <div className="LoginPage__loginButton">
                <Button variant="contained" endIcon={<ArrowForwardRoundedIcon />} color="primary" onClick={handleLogin}>Login</Button>
              </div>
            </div>
          </div>
          <Snackbar
            open={showSnack}
            onClose={() => setShowSnack(false)}
            anchorOrigin={{vertical: 'top', horizontal: 'center'}}
          >
            <MuiAlert elevation={6} variant="filled" onClose={() => setShowSnack(false)} severity="error">
              {errorMessage}
            </MuiAlert>
          </Snackbar>
        </Paper>
      </div>

    </div>
  );

  async function handleLogin() {
    await login({username, password}, (error) => {
      setErrorMessage(error);
      setShowSnack(true);
    });
  }
};

const mapStateToProps = (state) => ({
  admin: state.admin
});

const mapDispatchToProps = (dispatch) => ({
  login: bindActionCreators(login, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);