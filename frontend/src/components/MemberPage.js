import React      from 'react';
import './MemberPage.css';
import Paper      from '@material-ui/core/Paper';
import Container  from '@material-ui/core/Container';
import MemberForm from './MemberForm';
import PersonIcon from '@material-ui/icons/Person';
import Fab        from '@material-ui/core/Fab';
import SiteMenu       from './SiteMenu';

const MemberPage = () => {
  return (
    <div className="MemberPage">
      <SiteMenu />
      <div className="MemberPage__content">
        <Paper elevation={3} className="MemberPage__paper">
          <MemberForm />
        </Paper>
      </div>
      {/*<Container maxWidth="xs" className="MemberPage__content">*/}

      {/*</Container>*/}
    </div>
  )
};

export default MemberPage;