const moment = require('moment-timezone');
const Logs = require('./collections/logs');
const CONFIG = require('./stakeConfig.json');

async function generateReport(doc, params) {
  const {date, wards, reasons, chapelText, logText, chapel} = params;
  const day = moment(parseInt(date)).tz('Australia/Brisbane');
  const startOfDay = day.startOf('day').valueOf();
  const endOfDay = day.endOf('day').valueOf();
  const logs = await Logs.find({date: {$gt: startOfDay, $lt: endOfDay}, ward: {$in: wards}, reason: {$in: reasons}, chapel}).lean(); // get this value sending
  const formattedDate = day.format('D MMM YYYY');
  doc
    .font('./assets/fonts/Calibri.woff')
    .fill('#294c6e')
    .fontSize(30)
    .text('', 40, 40)
    .text(`${chapelText} - `, {continued: true})
    .fontSize(30)
    .text(logText)
    .fontSize(16)
    .text('', 680, 53)
    .text(formattedDate, {width: 119, align: 'right'})
    .moveTo(40, 92)
    .lineTo(801, 92)
    .stroke('#1e3b63');

  insertFooter(doc, logs.length);

  doc.on('pageAdded', () => {
    generateHeaderFooter(doc, formattedDate, logs.length, chapelText, logText);
    insertTableHeader(doc)
  });

  await addLogsToReport(logs, doc);

  generatePageNumbers(doc);
}

function insertFooter(doc, totalCount) {
  doc
    .moveTo(40, 555)
    .lineTo(801, 555)
    .stroke('#1e3b63')
    .fontSize(13)
    .text('', 40, 560)
    .text(`${CONFIG.STAKE_NAME} - ${CONFIG.STAKE_URL}`, {width: 200})
    .text('', 600, 560)
    .text(`Total Records: ${totalCount}`, {width: 200, align: 'right'});
}

async function addLogsToReport(logs, doc) {
  insertTableHeader(doc, true);

  logs.map(aLog => {
    if (doc.y > 525) {
      doc.addPage();

      doc.y = 110;
    }

    insertRecord(aLog, doc, doc.y < 500);
  });

}

function insertTableHeader(doc, firstPage) {
  const yPosition = firstPage ? 103 : 90;

  // margin lines
  // doc
  //   .moveTo(40, yPosition)
  //   .lineTo(90, yPosition)
  //   .stroke('red');
  //
  // doc
  //   .moveTo(100, yPosition)
  //   .lineTo(260, yPosition)
  //   .stroke('blue');
  //
  // doc
  //   .moveTo(270, yPosition)
  //   .lineTo(360, yPosition)
  //   .stroke('green');
  //
  // doc
  //   .moveTo(370, yPosition)
  //   .lineTo(610, yPosition)
  //   .stroke('purple');
  //
  //
  // doc
  //   .moveTo(620, yPosition)
  //   .lineTo(690, yPosition)
  //   .stroke('pink');
  //
  //
  // doc
  //   .moveTo(700, yPosition)
  //   .lineTo(801, yPosition)
  //   .stroke('yellow');



  // table headers
  doc
    .fontSize(11)
    .text('', 40, yPosition)
    .text('Time', {width: 50})
    .text('', 100, yPosition)
    .text('Name', {width: 160})
    .text('', 270, yPosition)
    .text('Phone Number', {width: 90})
    .text('', 370, yPosition)
    .text('Address', {width: 240})
    .text('', 620, yPosition)
    .text('Ward', {width: 75})
    .text('', 700, yPosition)
    .text('Purpose', {width: 101});

  doc
    .moveTo(40, yPosition + 19)
    .lineTo(801, yPosition + 19)
    .stroke('#1e3b63');

  doc.y = yPosition + 19;

}

function insertRecord(aLog, doc, showLine) {
  const yPosition = doc.y + 12;
  let rowBottom = doc.y;

  doc
    .fontSize(11)
    .text('', 40, yPosition)
    .text(moment(aLog.date).tz('Australia/Brisbane').format('h:mm a'), {width: 50});
  rowBottom = Math.max(rowBottom, doc.y);

  doc
    .text('', 100, yPosition)
    .text(aLog.name, {width: 160});
  rowBottom = Math.max(rowBottom, doc.y);

  doc
    .text('', 270, yPosition)
    .text(aLog.phone, {width: 90});
  rowBottom = Math.max(rowBottom, doc.y);

  doc
    .text('', 370, yPosition)
    .text(aLog.address, {width: 240});
  rowBottom = Math.max(rowBottom, doc.y);

  doc
    .text('', 620, yPosition)
    .text(aLog.ward, {width: 75});
  rowBottom = Math.max(rowBottom, doc.y);

  doc
    .text('', 700, yPosition)
    .text(aLog.reason, {width: 101});
  rowBottom = Math.max(rowBottom, doc.y);

  doc.y = rowBottom + 5;

  if (showLine) {
    // draw line
    doc
      .moveTo(40, doc.y)
      .lineTo(801, doc.y)
      .stroke('#cedaeb');
  }

}

function generateHeaderFooter(doc, formattedDate, totalCount, chapelText, logText) {
  doc
    .fill('#294c6e')
    .fontSize(16)
    .text('', 40, 40)
    .text(`${chapelText} - `, {continued: true})
    .fontSize(16)
    .text(logText)
    .fontSize(14)
    .text('', 680, 43)
    .text(formattedDate, {width: 119, align: 'right'})
    .moveTo(40, 80)
    .lineTo(801, 80)
    .stroke('#1e3b63');

  insertFooter(doc, totalCount);
}

function generatePageNumbers(doc) {
  const range = doc.bufferedPageRange(); // => { start: 0, count: 2 }
  let i;
  for (i = 0; i < range.count; i++) {
    doc.switchToPage(i);
    doc
      .fontSize(13)
      .text('', 0, 560)
      .text(`Page ${i + 1} of ${range.count}`, {align: 'center'});
  }
}

module.exports = {
  generateReport,
};