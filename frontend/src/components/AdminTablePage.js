import DateFnsUtils                                   from '@date-io/date-fns';
import Button                                         from '@material-ui/core/Button';
import CircularProgress                               from '@material-ui/core/CircularProgress';
import Grow                                           from '@material-ui/core/Grow';
import IconButton                                     from '@material-ui/core/IconButton';
import Paper                                          from '@material-ui/core/Paper';
import ArrowBackIosIcon                               from '@material-ui/icons/ArrowBackIos';
import AssignmentIcon                                 from '@material-ui/icons/Assignment';
import {KeyboardDatePicker, MuiPickersUtilsProvider,} from '@material-ui/pickers';
import 'date-fns';
import moment                                         from 'moment';
import React, {useEffect, useState}                   from 'react';
import {connect}                                      from 'react-redux';
import {useHistory}                                   from 'react-router-dom';
import {bindActionCreators}                           from 'redux';
import {generateReport, getLogs, logout}              from '../actions/admin';
import CONFIG                                         from './../stakeConfig.json';
import './AdminTablePage.css';
import LogFilter                                      from './LogFilter';
// import socket                              from '../socket';
import LogRow                                         from './LogRow';
import SiteMenu                                       from './SiteMenu';

const AdminTablePage = ({state, admin, loggedIn, getLogs, generateReport}) => {
  const {pastLogs, todayLogs} = admin;
  const history = useHistory();
  const [date, setDate] = useState(moment().startOf('day').valueOf());
  const [tableRef, setTableRef] = useState(null);
  const [tableHeight, setTableHeight] = useState(resizeTable);
  const [wards, setWards] = useState(getWards());
  const [reasons, setReasons] = useState(CONFIG.REASONS);

  const today = moment().startOf('day').valueOf();
  let logs = today === date ? todayLogs : pastLogs;

  // make sure we only see this chapels logs
  logs = logs.filter(aLog => filterALog(aLog));

  // active count
  const activeCount = logs.filter(aLog => aLog.active).length;

  const chapelText = getChapelText();
  const logText = getLogText();

  useEffect(() => {
    if (!loggedIn) history.push('/login');
  }, [loggedIn]);

  useEffect(() => {
    if (!loggedIn) return;

    setTimeout(function() {
      resizeTable();
    }, 200);
  }, [pastLogs, logs]);

  return (
    <div className="AdminTablePage">
      <SiteMenu/>
      <Paper elevation={3} className="AdminTablePage__paper">
        {admin.stakeAccess &&
          <div className="AdminTablePage__backButton">
            <Button size="small" startIcon={<ArrowBackIosIcon />} color="primary" onClick={handleBack}>Back</Button>
          </div>
        }
        {today === date &&
          <div className="AdminTablePage__activeCount">{activeCount}</div>
        }
        <div className="AdminTablePage__title">
          {chapelText}
        </div>
        <div className="AdminTablePage__subTitle">
          {logText} - {`Total: ${logs.length}`}
        </div>
        <div className="AdminTablePage__actionRow">
          <div className="AdminTablePage__actions">
            {admin.reportLoading &&
              <div className="AdminTablePage__reportLoading">
                <CircularProgress size={22} />
              </div>
            }
            {!admin.reportLoading &&
              <IconButton color="primary" disabled={logs.length === 0} component="span" className="AdminTablePage__actionButton" onClick={handleGenerateReport}>
                <AssignmentIcon />
              </IconButton>
            }
            <LogFilter wards={wards} availableWards={getWards()} setWards={setWards} reasons={reasons} setReasons={setReasons} showWards={admin.stakeAccess} />
          </div>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
              margin="normal"
              label=""
              format="d MMMM yyyy"
              value={date}
              onChange={handleDateChange}
              InputProps={{
                disableUnderline: true,
              }}
              className="AdminTablePage__datePicker"
            />
          </MuiPickersUtilsProvider>
        </div>

        <div className="AdminTablePage__table" ref={(r) => setTableRef(r)} style={{minHeight: tableHeight}}>
          {logs.map((aLog, index) => {
            return (
              <Grow in={!!(index + 1)} timeout={1} onEntered={resizeTable} key={index} >
                <LogRow updateHeight={resizeTable} log={aLog} last={index === 0} />
              </Grow>
            )
          })}
          {logs.length === 0 &&
            <div className="AdminTablePage__noLogs">
              No entries
            </div>
          }
        </div>

      </Paper>
    </div>
  );

  function filterALog(aLog) {
    let reasonFilter = reasons.includes(aLog.reason);

    if (reasons.includes('Youth Activities') && aLog.reason.includes('Youth Activities')) reasonFilter = true;
    if (reasons.includes('YSA Activities') && aLog.reason.includes('YSA Activities')) reasonFilter = true;

    return aLog.chapel === admin.showChapel && wards.includes(aLog.ward) && reasonFilter;
  }

  function getWards() {
    // const chapel = CONFIG.CHAPEL_WARDS.find(aChapel => aChapel.NAME === admin.showChapel);

    if (admin.stakeAccess) {
      return CONFIG.WARDS;
    }

    return [admin.ward];
  }

  function getChapelText() {
    let text = `${admin.showChapel} Chapel`;

    if (wards.length === 1) {
      text = `${wards[0]} Ward`;
    }

    return text;
  }

  function getLogText() {
    const chapelWards = getWards();
    const oneWardOrAll = wards.length === chapelWards.length || wards.length === 1;

    // should all rows or filtered show
    let isNotFiltering = oneWardOrAll && reasons.length === CONFIG.REASONS.length;

    let logText = isNotFiltering ? 'All Logs' : 'Filtered Logs';

    if (oneWardOrAll && reasons.length === 1) {
      logText = reasons[0];
    }

    return logText;
  }


  function resizeTable() {
    if (!tableRef) return;
    setTimeout(function() {
      let height = 0;
      [...tableRef.children].forEach(aChild => height += aChild.offsetHeight);
      setTableHeight(height);
    }, 100);
  }

  async function handleBack() {
    await CloseSocket();
    history.push('/dashboard');
  }

  async function CloseSocket() {
    // await socket.close();
  }

  function handleDateChange(aDate) {
    const dateSelected = moment(aDate).startOf('day').valueOf();
    setDate(dateSelected);
    if (dateSelected !== today)  {
      // get logs from previous day
      getLogs(dateSelected, () => {
        resizeTable();
      });
      return;
    }
    resizeTable();
  }

  function handleGenerateReport() {
    generateReport({date, wards, reasons, chapelText, logText, chapel: admin.showChapel})
  }
};

const mapStateToProps = (state) => ({
  state,
  admin: state.admin,
  loggedIn: state.admin.loggedIn,
});

const mapDispatchToProps = (dispatch) => ({
  logout: bindActionCreators(logout, dispatch),
  getLogs: bindActionCreators(getLogs, dispatch),
  generateReport: bindActionCreators(generateReport, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AdminTablePage);