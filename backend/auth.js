const jwt = require('jsonwebtoken');
const Users = require('./collections/users');
const moment = require('moment-timezone');


module.exports = function(req, res, next) {
  jwt.verify(req.headers.authorization, 'icodhw2020', async (err, decoded) => {
    if (err) {
      res.status(401).send({message: 'You do not have access.'});
      return;
    }

    const user = await Users.findById(decoded._id).lean();
    const beenOneDay = moment(decoded.date).tz('Australia/Brisbane').add(1, 'days').valueOf() <= moment().tz('Australia/Brisbane').valueOf();

    if (!user) {
      res.status(401).send({message: 'You are no longer logged in.'});
      return;
    }

    next();

  });
};