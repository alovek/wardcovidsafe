import React, {useEffect} from 'react';
import {connect}          from 'react-redux';
import {bindActionCreators}       from 'redux';
import io                         from "socket.io-client";
import {logout, updateAdminState} from '../actions/admin';
let socket = io(process.env.REACT_APP_SOCKET_URL, {autoConnect: false});

const Socket = ({admin, updateAdminState}) => {
  useEffect(() => {
    if (!admin.loggedIn) {
      socket.close();
      return;
    }

    InitialiseSocket();
  }, [admin.loggedIn]);

  return (<div></div>)

  function InitialiseSocket() {
    socket.open();

    socket.on('connect', () => {
      socket.emit('authorise', {token: admin.token});
    });
    socket.on('log update', (data) => {
      updateAdminState({todayLogs: data})
    });
  }
};

const mapStateToProps = (state) => ({
  state,
  admin: state.admin,
  loggedIn: state.admin.loggedIn,
});

const mapDispatchToProps = (dispatch) => ({
  logout: bindActionCreators(logout, dispatch),
  updateAdminState: bindActionCreators(updateAdminState, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Socket);
