import Button               from '@material-ui/core/Button';
import Paper                from '@material-ui/core/Paper';
import CloseIcon            from '@material-ui/icons/Close';
import React, {useState}    from 'react';
import './LogRow.css';
import Grid                 from '@material-ui/core/Grid';
import moment               from 'moment';
import Collapse             from '@material-ui/core/Collapse';
import {connect}            from 'react-redux';
import {bindActionCreators}         from 'redux';
import {deleteLog, getLogs, logout} from '../actions/admin';
import {ACCESS_LEVEL}               from '../constants';

const LogRow = ({log, last, admin, deleteLog}) => {
  const [expand, setExpand] = useState(false);

  const showDelete = log.active || [ACCESS_LEVEL.CREATOR, ACCESS_LEVEL.SUPER_ADMIN, ACCESS_LEVEL.STAKE_ADMIN].includes(admin.accessLevel);

  return (
    <React.Fragment>
      <div className={`LogRow ${log.active ? 'LogRow--active' : ''} ${last ? 'LogRow--last' : ''}`}>
        <div className="LogRow__defaultRow" onClick={() => setExpand(!expand)}>
          <div className="LogRow__date">{moment(parseInt(log.date)).format('h:mma')}</div>
          <div className="LogRow__name">{log.name}</div>
          <div className="LogRow__ward">{log.ward}</div>
          <div className="LogRow__reason">{log.reason}</div>
        </div>
        <Collapse in={expand}>
          <div className="LogRow__expandedRow">
            <div className="LogRow__expandedRowItem LogRow__mobileOnly">
              <div className="LogRow__itemTitle">Ward</div>
              <div>{log.ward}</div>
            </div>
            <div className="LogRow__expandedRowItem LogRow__mobileOnly">
              <div className="LogRow__itemTitle">Reason</div>
              <div>{log.reason}</div>
            </div>
            <div className="LogRow__expandedRowItem">
              <div className="LogRow__itemTitle">Address</div>
              <div>{log.address}</div>
            </div>
            <div className="LogRow__expandedRowItem">
              <div className="LogRow__itemTitle">Phone Number</div>
              <div>{log.phone}</div>
            </div>
            {showDelete &&
              <div className="LogRow__delete">
                <Button size="small" variant="contained" endIcon={<CloseIcon/>} onClick={handleDelete} color="secondary">Delete</Button>
              </div>
            }
          </div>
        </Collapse>
      </div>
    </React.Fragment>
  );

  function handleDelete() {
    setExpand(false);
    deleteLog(log._id);
  }
};


const mapStateToProps = (state) => ({
  state,
  admin: state.admin,
  loggedIn: state.admin.loggedIn,
});

const mapDispatchToProps = (dispatch) => ({
  logout: bindActionCreators(logout, dispatch),
  getLogs: bindActionCreators(getLogs, dispatch),
  deleteLog: bindActionCreators(deleteLog, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(LogRow);

