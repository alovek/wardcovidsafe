
const initialState = {
  loggedIn: false,
  token: null,
  name: '',
  accessLevel: null,
  ward: '',
  stakeAccess: false,
  showChapel: '',
  pastLogs: [],
  todayLogs: [],
  reportLoading: false,
};

export default (state = initialState, action) => {
  switch(action.type) {
    case 'LOGGED_IN':
      return {...state, ...action.data};
    case 'UPDATE_ADMIN_FIELDS':
      return {...state, ...action.data};
    case 'DELETE_LOG':
      let updatedPastLogs = [...state.pastLogs];
      let updatedTodayLogs = [...state.todayLogs];
      updatedPastLogs = updatedPastLogs.filter(aLog => aLog._id !== action.data.id);
      updatedTodayLogs = updatedTodayLogs.filter(aLog => aLog._id !== action.data.id);
      return {...state, pastLogs: updatedPastLogs, todayLogs: updatedTodayLogs};
    case 'LOGGED_OUT':
      return {...initialState};
    case 'REPORT_LOADING':
      return {...state, reportLoading: action.data};
    default:
      return state;
  }
}