import axios from 'axios';

const backendAPI = axios.create({
  baseURL: `${process.env.REACT_APP_API_URL}`,
});

backendAPI.interceptors.request.use(config => {
  config.headers.post['header1'] = 'value';

  return config;
});

export default backendAPI;