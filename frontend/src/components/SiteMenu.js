import Fab                  from '@material-ui/core/Fab';
import MenuIcon             from '@material-ui/icons/Menu';
import React                from 'react';
import Menu                 from '@material-ui/core/Menu';
import MenuItem             from '@material-ui/core/MenuItem';
import './SiteMenu.css';
import {connect}            from 'react-redux';
import { useHistory }       from "react-router-dom";
import {bindActionCreators} from 'redux';
import {logout}             from '../actions/admin';
import {updateField}        from '../actions/member';
import {CHAPELS}            from '../stakeConfig';

const SiteMenu = ({admin, member, updateField, logout}) => {
  const {loggedIn, stakeAccess} = admin;
  const history = useHistory();
  const [anchorEl, setAnchorEl] = React.useState(null);

  return (
    <div className="SiteMenu">
      <div className="SiteMenu__memberIcon">
        <Fab color="primary" size="medium" className="" onClick={handleClick}>
          <MenuIcon />
        </Fab>
      </div>
      {!loggedIn &&
        <Menu
          anchorEl={anchorEl}
          keepMounted
          open={!!anchorEl}
          onClose={handleClose}
        >
          <MenuItem onClick={handleLogin}>Login</MenuItem>
          {getChapelMenuItems()}
        </Menu>
      }
      {loggedIn &&
        <Menu
          anchorEl={anchorEl}
          keepMounted
          open={!!anchorEl}
          onClose={handleClose}
        >
          {stakeAccess
            ? <MenuItem onClick={handleDashboard}>Dashboard</MenuItem>
            : <MenuItem onClick={handleTable}>Chapel Logs</MenuItem>
          }

          {/*<MenuItem onClick={handleClose}>My account</MenuItem>*/}
          {getChapelMenuItems()}
          <MenuItem onClick={handleLogout}>Logout</MenuItem>
        </Menu>
      }
    </div>
  );

  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  async function handleChapel(aChapel) {
    await updateField({field: 'chapel', value: aChapel});
    handleClose();
    history.push(`/?chapel=${aChapel}`);
  }

  function handleTable() {
    handleClose();
    history.push('/chapel-logs');
  }

  function handleDashboard() {
    handleClose();
    history.push('/dashboard');
  }

  function handleLogin() {
    handleClose();
    history.push('/login');
  }

  function handleLogout() {
    logout();
    handleClose();
  }

  function getChapelMenuItems() {
    return CHAPELS.map((aChapel, index) => {
      const chapelMenuName = loggedIn ? `Check in ${aChapel}` : `${aChapel} Chapel`;
      return <MenuItem key={index} onClick={() => handleChapel(aChapel)}>{chapelMenuName}</MenuItem>
    })
  }

};


const mapStateToProps = (state) => ({
  admin: state.admin,
  member: state.member,
});

const mapDispatchToProps = (dispatch) => ({
  updateField: bindActionCreators(updateField, dispatch),
  logout: bindActionCreators(logout, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(SiteMenu);
