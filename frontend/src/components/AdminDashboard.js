import ButtonBase                          from '@material-ui/core/ButtonBase';
import Grow                                from '@material-ui/core/Grow';
import Paper                               from '@material-ui/core/Paper';
import React, {useEffect, useState}        from 'react';
import {connect}                           from 'react-redux';
import {Redirect, useHistory, useLocation} from 'react-router-dom';
import './AdminDashboard.css';
import {bindActionCreators}              from 'redux';
import {login, logout, updateAdminState} from '../actions/admin';
import SiteMenu                          from './SiteMenu';
import Grid from '@material-ui/core/Grid';
import Slide                               from '@material-ui/core/Slide';
import moment from 'moment';
import CONFIG from '../stakeConfig.json';
import chapelImage from '../assets/images/chapel.jpg';


const AdminDashboard = ({state, admin, loggedIn, updateAdminState}) => {
  const history = useHistory();

  useEffect(() => {
    if (!loggedIn) history.push('/login');
  }, [loggedIn]);


  return (
    <div className="AdminDashboard">
      <SiteMenu/>
      <Paper elevation={3} className="AdminDashboard__paper">
        <div className="AdminDashboard__title">{CONFIG.STAKE} Stake Leadership</div>
        <Grid container spacing={8}>
          {CONFIG.CHAPELS.map((aChapel, index) => {
            return (
              <Grid item xs={12} lg={6} key={index}>
                <ButtonBase
                  focusRipple
                  className="AdminDashboard__button"
                  onClick={() => handleAdminTable(aChapel)}
                >
                  <div className="AdminDashboard__button">{aChapel} Chapel</div>
                </ButtonBase>
              </Grid>
            );
          })}
        </Grid>
      </Paper>
    </div>
  );

  async function handleAdminTable(aChapel) {
    await updateAdminState({showChapel: aChapel});
    history.push('/chapel-logs');
  }
};

const mapStateToProps = (state) => ({
  state,
  admin: state.admin,
  loggedIn: state.admin.loggedIn,
});

const mapDispatchToProps = (dispatch) => ({
  logout: bindActionCreators(logout, dispatch),
  updateAdminState: bindActionCreators(updateAdminState, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AdminDashboard);