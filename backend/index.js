process.env.NODE_ENV = process.env.NODE_ENV || 'development';
const mongoose = require('mongoose');
const express = require('express');
const moment = require('moment-timezone');
const PDFDocument = require('pdfkit');
const app = express();
const Logs = require('./collections/logs');
const Users = require('./collections/users');
const cors = require('cors');
const jwt = require('jsonwebtoken');
app.use(express.json());
app.use(cors());
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const CONFIG = require('./stakeConfig.json');
const {ACCESS_LEVEL} = require('./constants');
const auth = require('./auth');
const helper = require('./helper');

const {CronJob} = require('cron');

new CronJob('*/5 * * * *', async function() {
  const activeLogs = await Logs.find({active: true});
  await Promise.all(activeLogs.map(async aLog => {
    const notActive = moment(aLog.date).tz('Australia/Brisbane').add(2, 'hours').valueOf() <= moment().tz('Australia/Brisbane').valueOf();

    if (notActive) {
      await Logs.findByIdAndUpdate(
        aLog._id,
        { $set: { active: false } }
      );
      await UpdateRooms();
    }
  }));

}, null, true, 'Australia/Brisbane', {}, true);



io.on('connection', (client) => {
  client.on('authorise', data => {
    jwt.verify(data.token, 'icodhw2020', async (err, decoded) => {
      if (err) return;

      const user = await Users.findById(decoded._id).lean();

      if (!user) return;

      if ([ACCESS_LEVEL.STAKE_VIEW, ACCESS_LEVEL.STAKE_ADMIN, ACCESS_LEVEL.SUPER_ADMIN, ACCESS_LEVEL.CREATOR].includes(user.accessLevel)) {
        // add to stake room
        await client.join('Stake');
        await UpdateRooms();
        return;
      }

      await Promise.all(CONFIG.CHAPELS.map(async aChapel => {
        if (aChapel.WARDS.includes(user.ward)) {
          // add to this chapel room
          await client.join(aChapel.NAME);
          await UpdateRooms();
          return;
        }
      }));
    });
  });
  client.on('disconnect', () => {

  });
});

async function UpdateRooms() {
  const today = moment().tz('Australia/Brisbane');
  const startOfDay = today.startOf('day').valueOf();
  const endOfDay = today.endOf('day').valueOf();
  const logs = await Logs.find({date: {$gt: startOfDay, $lt: endOfDay}}).lean();
  await io.to('Stake').emit('log update', [...logs]);

  await Promise.all(CONFIG.CHAPELS.map(async aChapel => {
    const chapelLogs = logs.filter(aLog => aChapel.WARDS.includes(aLog.ward));
    await io.to(aChapel.NAME).emit('log update', [...chapelLogs]);
  }));

}

const uri = `mongodb://localhost:27017/${CONFIG.DATABASE}`;

mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true });

const connection = mongoose.connection;

app.get('/logs', auth, async (req, res) => {
  const payload = req.query;
  const day = moment(parseInt(payload.date)).tz('Australia/Brisbane');
  const startOfDay = day.startOf('day').valueOf();
  const endOfDay = day.endOf('day').valueOf();
  const logs = await Logs.find({date: {$gt: startOfDay, $lt: endOfDay}}).lean();
  res.send({logs});
});

app.get('/', async (req, res) => {
  res.send({stake: CONFIG.STAKE_NAME});
});

app.get('/generate-report', auth, async (req, res) => {
  const payload = req.query;

  const doc = new PDFDocument({size: 'A4', margin: 0, bufferPages: true, layout : 'landscape'});

  res.statusCode = 200;
  res.setHeader('Content-type', 'application/pdf');
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Content-disposition', 'attachment; filename=Untitled.pdf');

  doc.pipe(res);
  await helper.generateReport(doc, payload);
  doc.end();
});

app.post('/deactivate-log', async (req, res) => {
  const {checkedInIds=[]} = req.body;

  for (const id of checkedInIds) {
    await Logs.updateOne({_id: id}, {active: false});
  }

  await UpdateRooms();
  res.send({status: 'success'});
});

app.post('/delete-log/:logId', auth, async (req, res) => {
  const {logId} = req.params;
  await Logs.deleteOne({_id: logId});
  res.send({status: 'success'});
});

app.post('/login', async (req, res) => {
  const {username, password} = req.body;
  const user = await Users.findOne({username: username.toLowerCase(), password}).lean();

  if (!user) {
    res.send({error: 'Incorrect username or password.'});
    return;
  }

  const token = jwt.sign({_id: user._id, date: moment().tz('Australia/Brisbane').valueOf()}, 'icodhw2020');
  res.send({user, token, message: 'Login Successful'});
  console.log('someone logged in', req.body);

});

app.post('/add-user', async (req, res) => {
  const user = await Users.create(req.body);
  res.send(user);
});

app.post('/log', async (req, res) => {
  const payload = req.body;
  const mainDetailsObj = {
    phone: payload.phone,
    address: payload.address,
    reason: payload.reason,
    ward: payload.ward,
    chapel: payload.chapel,
    date: moment().tz('Australia/Brisbane').valueOf(),
    formattedDate: moment().tz('Australia/Brisbane').format('dddd DD MMMM YYYY hh:mm a'),
    active: true,
  };

  const logs = [];

  [payload.name, ...payload.additionalNames].map(aName => {
    logs.push({...mainDetailsObj, name: aName})
  });

  const checkedInIds = [];

  for (const aLog of logs) {
    const logId = await Logs.create(aLog);
    checkedInIds.push(logId._id);
  }

  await UpdateRooms();
  res.send({status: 'success', checkedInIds})
});

connection.once('open', function() {
  console.log('MongoDB database connection established successfully');
});


server.listen(CONFIG.PORT, function() {
  console.log('Server is running on Port: ' + CONFIG.PORT);
});