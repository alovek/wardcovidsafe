import backendAPI         from '../api';
import {ACCESS_LEVEL}     from '../constants';
import CONFIG             from '../stakeConfig.json';

export const login = (payload, callback) => async (dispatch, getState) => {
  const response = await backendAPI.post('/login', payload);
  const {data} = response;

  // if something went wrong show it
  if (data.error) {
    callback(data.error);
    return;
  }
  const {user, token} = data;
  const {accessLevel, name, ward} = user;
  const stakeAccess = [ACCESS_LEVEL.STAKE_VIEW, ACCESS_LEVEL.STAKE_ADMIN, ACCESS_LEVEL.SUPER_ADMIN, ACCESS_LEVEL.CREATOR].includes(accessLevel);
  let showChapel = '';
  CONFIG.CHAPEL_WARDS.map(aChapel => {
    if (aChapel.WARDS.includes(ward)) {
      showChapel = aChapel.NAME;
    }
  });

  // update the store
  dispatch({
    type: 'LOGGED_IN',
    data: {token, accessLevel, name, ward, loggedIn: true, stakeAccess, showChapel},
  });
};

export const getLogs = (date, callback) => async (dispatch, getState) => {
  const {token} = getState().admin;
  let response;

  try {
    response = await backendAPI.get(`/logs?date=${date}`, {headers: {authorization: token}});
  } catch {
    dispatch(logout());
    return;
  }
  const {data} = response;

  dispatch({
    type: 'UPDATE_ADMIN_FIELDS',
    data: {pastLogs: data.logs}
  });

  callback();
};

export const deleteLog = (id) => async (dispatch, getState) => {
  const {token} = getState().admin;

  try {
    await backendAPI.post(`/delete-log/${id}`, {}, {headers: {authorization: token}});
  } catch {
    dispatch(logout());
    return;
  }

  dispatch({
    type: 'DELETE_LOG',
    data: {id}
  });
};

export const generateReport = (params) => async (dispatch, getState) => {
  const {token} = getState().admin;
  let response;

  try {
    dispatch({
      type: 'REPORT_LOADING',
      data: true,
    });
    response = await backendAPI.get(`/generate-report`, {headers: {authorization: token}, responseType: 'blob', params});
  } catch {
    // dispatch(logout());
    return;
  }
  dispatch({
    type: 'REPORT_LOADING',
    data: false,
  });
  const url = window.URL.createObjectURL(new Blob([response.data], {type: 'application/pdf'}));
  window.open(url);
};

export const logout = (payload, callback) => async (dispatch, getState) => {
  // update the store
  dispatch({
    type: 'LOGGED_OUT',
  });
};

export const updateAdminState = (data) => async (dispatch, getState) => {
  // update the store
  dispatch({
    type: 'UPDATE_ADMIN_FIELDS',
    data
  });
};