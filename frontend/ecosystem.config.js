const CONFIG = require('./src/stakeConfig.json');


module.exports = {
  apps : [{
    name: `${CONFIG.STAKE}-FRONTEND`,
    script: 'npx',
    args: `serve -s build -l ${CONFIG.PORT}`,
    env_production : {
      NODE_ENV: 'production'
    },
    watch: '.'
  }],
};
