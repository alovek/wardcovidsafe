import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import IconButton from '@material-ui/core/IconButton';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Snackbar from '@material-ui/core/Snackbar';
import TextField from '@material-ui/core/TextField';
import AddIcon from '@material-ui/icons/Add';
import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';
import MuiAlert from '@material-ui/lab/Alert';
import moment from 'moment';
import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {useLocation} from 'react-router-dom';
import {bindActionCreators} from 'redux';
import {clearForm, sendForm, updateField} from '../actions/member';
import churchLogo from '../assets/images/churchMainLogo.svg';
import CONFIG from '../stakeConfig.json';
import CompletedCheckIn from './CompletedCheckIn';
import Autocomplete from '@material-ui/lab/Autocomplete';
import './MemberForm.css';

const MemberForm = ({member, updateField, sendForm, clearForm}) => {
  const queryParams = new URLSearchParams(useLocation().search);

  useEffect(() => {
    (async () => {
      let chapel =  CONFIG.DEFAULT_CHAPEL;
      const chapelQueryParam = queryParams.get('chapel');
      if (chapelQueryParam && CONFIG.CHAPELS.includes(chapelQueryParam)) {
        chapel = chapelQueryParam;
      }
      await updateField({field: 'chapel', value: chapel});
    })();
  }, []);

  const [showSnack, setShowSnack] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [addressResults, setAddressResults] = useState([]);

  // show the complete form if registered in the last 2 hours
  if (moment().valueOf() <= moment(member.lastDateSigned).add(2, 'hours').valueOf()) return <CompletedCheckIn />;

  const showAdditionalCheckboxes = member.reason === 'YSA Activities' || member.reason === 'Youth Activities';

  return (
    <div className="MemberForm">
      <div className="MemberForm__refreshButton">
        <IconButton color="primary" onClick={handleClearForm}>
          <ClearIcon fontSize="large" />
        </IconButton>
      </div>
      <div>
        <img src={churchLogo} />
      </div>
      <div className="MemberForm__chapelTitle">
        {member.chapel} Chapel
      </div>
      <div className="MemberForm__form">
        <TextField label="Name" fullWidth value={member.name} onChange={(e) => updateField({field: 'name', value: e.target.value})} />
        {member.additionalNames.map((aName, index) => {
          return (
            <div className="MemberForm__inputRow MemberForm__additionalName" key={index}>
              <TextField label={`Name ${index + 2}`} fullWidth value={aName} onChange={(e) => {
                const updatedAdditionalNames = [...member.additionalNames];
                updatedAdditionalNames[index] = e.target.value;
                updateField({field: 'additionalNames', value: updatedAdditionalNames})
              }} />
              <div className="MemberForm__deleteAdditionalName">
                <IconButton color="disabled" size="small" onClick={() => deleteAdditionalName(index)}>
                  <ClearIcon fontSize="small" />
                </IconButton>
              </div>
            </div>
          );
        })}
        <div className="MemberForm__anotherNameButton">
          <Button size="small" startIcon={<AddIcon />} color="primary" onClick={() => updateField({field: 'additionalNames', value: [...member.additionalNames, '']})}>Add</Button>
        </div>
        <div className="MemberForm__inputRow">
          <TextField label="Phone" fullWidth value={member.phone} type="number" onChange={(e) => updateField({field: 'phone', value: e.target.value})} />
        </div>
        <div className="MemberForm__inputRow">
          <Autocomplete
            options={addressResults}
            blurOnSelect
            clearOnBlur
            freeSolo
            getOptionLabel={(option) => option.description || ''}
            renderInput={(params) => {
              return (
                <div ref={params.InputProps.ref}>
                  <TextField
                    {...params.inputProps}
                    multiline
                    value={member.address}
                    label="Address"
                    fullWidth
                  />
                </div>
              );
            }}
            onInputChange={handleLocationSearch}
            onChange={(e, value) => handleLocationUpdate(value)}
          />
        </div>
        <div className="MemberForm__inputRow">
          <FormControl fullWidth>
            <InputLabel id="reason-select">Purpose</InputLabel>
            <Select
              labelId="reason-select"
              fullWidth
              value={member.reason}
              onChange={handleReasonSelected}
            >
              {CONFIG.REASONS.map((aReason, index) => (
                <MenuItem key={index} value={aReason}>{aReason}</MenuItem>
              ))}
            </Select>
          </FormControl>
        </div>
        {showAdditionalCheckboxes &&
          <React.Fragment>
            <div className="MemberForm__rememberDetails">
              <FormControlLabel
                control={
                  <Checkbox
                    checked={member.includeFood || false}
                    onChange={(e, checked) => updateField({field: 'includeFood', value: checked})}
                    color="primary"
                  />
                }
                label="Food & drinks"
              />
            </div>
            <div className="MemberForm__rememberDetails">
              <FormControlLabel
                control={
                  <Checkbox
                    checked={member.includeGames || false}
                    onChange={(e, checked) => updateField({field: 'includeGames', value: checked})}
                    color="primary"
                  />
                }
                label="Indoor games"
              />
            </div>
          </React.Fragment>
        }
        <div className="MemberForm__inputRow">
          <FormControl fullWidth>
            <InputLabel id="ward-select">Ward</InputLabel>
            <Select
              labelId="ward-select"
              fullWidth
              value={member.ward}
              onChange={(e) => updateField({field: 'ward', value: e.target.value})}
            >
              {CONFIG.WARDS.map((aWard, index) => (
                <MenuItem key={index} value={aWard}>{aWard}</MenuItem>
              ))}
            </Select>
          </FormControl>
        </div>
        <div className="MemberForm__rememberDetails">
          <FormControlLabel
            control={
              <Checkbox
                checked={member.rememberDetails || false}
                onChange={(e, checked) => updateField({field: 'rememberDetails', value: checked})}
                color="primary"
              />
            }
            label="Remember my details"
          />
        </div>
        <div className="MemberForm__sendButton">
          <Button variant="contained" endIcon={<CheckIcon />} color="primary" onClick={send}>Check In</Button>
        </div>
      </div>
      <Snackbar
        open={showSnack}
        onClose={() => setShowSnack(false)}
        anchorOrigin={{vertical: 'bottom', horizontal: 'center'}}
      >
        <MuiAlert elevation={6} variant="filled" onClose={() => setShowSnack(false)} severity="error">
          {errorMessage}
        </MuiAlert>
      </Snackbar>
    </div>
  );

  function handleLocationSearch(e) {
    const text = e && e.target && e.target.value;
    if (text || text === '') {
      updateField({field: 'address', value: text});
      const google = new window.google.maps.places.AutocompleteService();
      google.getPlacePredictions(
        { input: text, componentRestrictions: { country: 'au' } },
        (results) => setAddressResults(results || [])
      );
    }
  }

  function handleLocationUpdate(value) {
    if (!value) return;
    updateField({field: 'address', value: value.description});
  }

  function handleReasonSelected(e) {
    updateField({field: 'reason', value: e.target.value});
    updateField({field: 'includeFood', value: false});
    updateField({field: 'includeGames', value: false});
  }

  function deleteAdditionalName(index) {
    const updatedAdditionalNames = [...member.additionalNames];
    updatedAdditionalNames.splice(index, 1);
    updateField({field: 'additionalNames', value: updatedAdditionalNames})
  }

  function handleClearForm() {
    clearForm();
  }

  async function send() {
    const validated = validateForm();

    if (validated) {
      sendForm();
    }
  }

  function validateForm() {
    if (!member.name) {
      showError('Name');
      return;
    }

    // check for first and last name
    // const nameArray = String.prototype.trim(member.name).split(' ');
    let nameExists = true;
    // nameArray.forEach(aName => nameExists = nameExists && !!aName);

    if(!nameExists) {
      setErrorMessage(`Please enter a first and last name.`);
      setShowSnack(true);
      return;
    }

    if (!member.phone) {
      showError('Phone');
      return;
    }

    if (!member.address) {
      showError('Address');
      return;
    }

    if (!member.reason) {
      showError('Purpose');
      return;
    }

    if (!member.ward) {
      showError('Ward');
      return;
    }

    return true;
  }

  function showError(newMessage) {
    setErrorMessage(`Please fill out the ${newMessage} field.`);
    setShowSnack(true);
  }
};

const mapStateToProps = (state) => ({
  member: state.member,
});

const mapDispatchToProps = (dispatch) => ({
  updateField: bindActionCreators(updateField, dispatch),
  sendForm: bindActionCreators(sendForm, dispatch),
  clearForm: bindActionCreators(clearForm, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(MemberForm);