const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let Users = new Schema(
  {
    name: {type: String},
    ward: {type: String},
    username: {type: String},
    password: {type: String},
    accessLevel: {type: String},
  },
  { collection: "users" }
);

module.exports = mongoose.model("users", Users);