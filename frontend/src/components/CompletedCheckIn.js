import Button                  from '@material-ui/core/Button';
import TextField               from '@material-ui/core/TextField';
import AddIcon           from '@material-ui/icons/Add';
import React, {useState} from 'react';
import './CompletedCheckIn.css';
import {connect}               from 'react-redux';
import {bindActionCreators}                             from 'redux';
import {addAnotherHouseholdName, checkOutLogs, sendForm, updateField} from '../actions/member';
import moment                                           from 'moment';
import IconButton from '@material-ui/core/IconButton';
import ArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

const CompletedCheckIn = ({member, updateField, addAnotherHouseholdName, checkOutLogs}) => {
  const [showAdditionalNameField, setShowAdditionalNameField] = useState(false);
  const [showAddButton, setShowAddButton] = useState(true);
  const [newName, setNewName] = useState('');

  const peopleCount = member.additionalNames.length + 1;

  return (
    <div className="CompletedCheckIn">
      <div className="CompletedCheckIn__date">{moment(member.lastDateSigned).format('D MMMM YYYY')}</div>
      <div className="CompletedCheckIn__wardText">{member.ward} Ward</div>
      <div className="CompletedCheckIn__wardText">{member.reason}</div>
      <div className="CompletedCheckIn__peopleCount">{peopleCount}</div>
      <div className="CompletedCheckIn__nameList">
        {[member.name, ...member.additionalNames].map((aName, index) => (
          <div className="CompletedCheckIn__name" key={index}>{aName}</div>
        ))}
        {showAdditionalNameField &&
        <div className="CompletedCheckIn__nameField">
          <TextField label="Name" fullWidth value={newName} onChange={(e) => setNewName(e.target.value)} />
          <IconButton color="primary" aria-label="upload picture" size="small" component="span" className="CompletedCheckIn__saveName" onClick={handleNewNameSave}>
            <ArrowForwardRoundedIcon />
          </IconButton>
        </div>
        }
        {showAddButton &&
          <div className="CompletedCheckIn__addName">
            <Button size="small" startIcon={<AddIcon />} color="primary" onClick={handleShowNameField}>Household member</Button>
          </div>
        }
      </div>
      <div className="CompletedCheckIn__checkOut">
        <Button variant="contained" endIcon={<ExitToAppIcon />} color="primary" onClick={checkOutLogs}>Leaving</Button>
      </div>
    </div>
  );

  function handleShowNameField() {
    setShowAddButton(false);
    setShowAdditionalNameField(true);
  }

  function handleNewNameSave() {
    if (!newName) return;
    const updatedAdditionalNames = [...member.additionalNames, newName];
    updateField({field: 'additionalNames', value: updatedAdditionalNames});
    addAnotherHouseholdName(newName);
    setNewName('');
    setShowAdditionalNameField(false);
    setShowAddButton(true);
  }
};


const mapStateToProps = (state) => ({
  member: state.member,
});

const mapDispatchToProps = (dispatch) => ({
  checkOutLogs: bindActionCreators(checkOutLogs, dispatch),
  updateField: bindActionCreators(updateField, dispatch),
  sendForm: bindActionCreators(sendForm, dispatch),
  addAnotherHouseholdName: bindActionCreators(addAnotherHouseholdName, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(CompletedCheckIn);
