import {createMuiTheme}                         from '@material-ui/core/styles';
import {ThemeProvider}                          from '@material-ui/styles';
import React                                    from 'react';
import {Provider}                               from 'react-redux';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {PersistGate}                            from 'redux-persist/lib/integration/react';
import './App.css';
import AdminDashboard                           from './components/AdminDashboard';
import AdminTablePage                           from './components/AdminTablePage';
import LoginPage                                from './components/LoginPage';
import MemberPage                               from './components/MemberPage';
import Socket                                   from './socket';
import {persistor, store}                       from './store';
import CONFIG                                   from './stakeConfig.json';

function App() {
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <Socket />
            <Router>
              <Switch>
                <Route exact path="/">
                  <MemberPage />
                </Route>
                <Route path="/chapel-logs">
                  <AdminTablePage />
                </Route>
                <Route path="/dashboard">
                  <AdminDashboard />
                </Route>
                <Route path="/login">
                  <LoginPage />
                </Route>
              </Switch>
            </Router>
          </PersistGate>
        </Provider>
      </ThemeProvider>
    </div>
  );
}

const theme = createMuiTheme({
  palette: {
    primary: {main: '#294c6e'},
    secondary: {main: '#BA000D'},
  },
});


export default App;
