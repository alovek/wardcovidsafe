const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let Logs = new Schema(
  {
    name: {type: String},
    phone: {type: String},
    address: {type: String},
    reason: {type: String},
    ward: {type: String},
    chapel: {type: String},
    date: {type: Number},
    formattedDate: {type: String},
    active: {type: Boolean},
  },
  { collection: "logs" }
);

module.exports = mongoose.model("logs", Logs);